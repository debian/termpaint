#! /bin/bash

set -x
set -e

cp -t $AUTOPKGTEST_TMP/ tests/*.cpp tests/*.h tests/input_tests.json

cd $AUTOPKGTEST_TMP

cat > meson.build <<EOF
project('autopkgtest', ['c', 'cpp'],
  default_options : ['buildtype=debugoptimized', 'cpp_std=c++14', 'c_std=gnu11'])

termpaint_dep = dependency('termpaint')

catch2_dep = dependency('catch2', required : true)

if catch2_dep.version().version_compare('>=3.0')
  catch2_dep = [catch2_dep, declare_dependency(compile_args: ['-DCATCH3'])]
endif

testlib = static_library('testlib', 'catch_main.cpp', dependencies: [catch2_dep])

test_files = [
  'fingerprintingtests.cpp',
  'input_tests.cpp',
  'measurement_tests.cpp',
  'surface.cpp',
  'terminal_misc.cpp',
]

testtermpaint = executable('testtermpaint', test_files,
  link_with: [testlib],
  dependencies: [termpaint_dep, catch2_dep])

testtermpaint_env = environment()
testtermpaint_env.set('TERMPAINT_TEST_DATA', meson.current_source_dir())
test('testtermpaint', testtermpaint, timeout: 1200, env: testtermpaint_env)

EOF

if ! meson _build ; then
    cp -r _build/meson-logs  $AUTOPKGTEST_ARTIFACTS
    exit 1
fi
if ! meson test -C _build ; then
    cp -r _build/meson-logs  $AUTOPKGTEST_ARTIFACTS
    exit 1
fi

cp -r _build/meson-logs  $AUTOPKGTEST_ARTIFACTS || true
